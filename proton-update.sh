#! /bin/sh

function version_info() {
  local label=$(print_blue "$1:")
  local version=$(print_yellow $2)
  printf "%s %s\n" "$label" "$version"
}

function get_release_info() {
  local repo=$1
  local filter=$2
  local data=$(curl -s "$repo" |
    grep "$filter" |
    cut -d : -f 2,3 |
    tr -d \", |
    sed 's/^[ \t]*//;s/[ \t]*$//')
  printf "%q" $data
}

function update() {
  local repo=https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases/latest
  local user_home=$(bash -c "cd ~$(printf %q $USER) && pwd")
  local dir_steam="$user_home/.steam/root/compatibilitytools.d/"

  local dir_script=
  [ -L ${BASH_SOURCE[0]} ] &&
    dir_script="$(cd "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" &>/dev/null && pwd)" ||
    dir_script="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
  local dir_downloads="${dir_script}/downloads"
  local version_file="${dir_script}/latest"

  local version_installed
  local version_latest
  local name
  local url

  source "$dir_script/utils/format.sh"

  info "Preparing Proton GE update"
  mkdir -p "$dir_steam" "$dir_downloads"

  if [ -f "$version_file" ]; then
    version_installed=$(head -n 1 $version_file)
    version_info "current version" $version_installed
  fi

  info "Obtaining latest version information"
  version_latest=$(get_release_info "$repo" "tag_name")
  name=$(get_release_info "$repo" "name.*tar.gz")
  url=$(get_release_info "$repo" "browser_download_url.*tar.gz")

  version_info "latest version" $version_latest
  version_info "file name" $name
  version_info "download url" $url

  ([ -z "$version_latest" ] || [ -z $name ] || [ -z $url ]) &&
    warn "Warning: Could not obtain latest version information" &&
    return 1

  [ "$version_latest" = "$version_installed" ] &&
    info "Proton GE is already up to date" &&
    rm -rf "${dir_downloads}" &&
    return

  info "Installing $version_latest" &&
    info "Downloading $name" &&
    wget -P "$dir_downloads" "$url" &&
    info "Unpacking $name to $dir_steam" &&
    tar -xf "$dir_downloads/$name" -C "$dir_steam" \
      --checkpoint=10000 --checkpoint-action=echo="#%u: %T" &&
    info "Finishing update process" &&
    info "Storing version informatin" &&
    echo $version_latest >$version_file &&
    info "Removing downloaded files" &&
    rm -rf "${dir_downloads}"
}

update
