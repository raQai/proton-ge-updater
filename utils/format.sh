function print_blue() {
    local col_default=$(tput sgr0)
    local col_blue=$(tput setaf 4)
    printf "${col_blue}%s${col_default}" "$1"
}

function print_yellow() {
    local col_default=$(tput sgr0)
    local col_yellow=$(tput setaf 3)
    printf "${col_yellow}%s${col_default}" "$1"
}

function info() {
    local text=$(print_blue "$1")
    printf "%s\n" "$text"
}

function warn() {
    local text=$(print_yellow "$1")
    printf "%s\n" "$text"
}
