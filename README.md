# Proton GE Updater

Script to add the most recent Proton GE version to your
`.steam/root/compatibilitytools.d` folder.

## Setup

Clone this repository

```
git clone git@gitlab.com:raQai/proton-ge-updater.git
```

Ensure you add the proper access rights to the script

```
$ chmod +x proton-update.sh
```


### Optional

Create symlink for direct access
```
$ ln -s /path/to/proton-ge-updater/proton-update.sh /usr/bin/proton-update
```
